# Generated by Django 2.1.4 on 2019-03-01 23:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0020_account_external_account_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='account',
            name='external_account_id',
            field=models.CharField(max_length=150, unique=True),
        ),
    ]
